import logging, select, subprocess,os
from datetime import datetime
import concurrent.futures
import time
from artifactory import ArtifactoryPath


dateTag = datetime.now().strftime("%Y-%b-%d_%H-%M-%S")
logging.basicConfig(filename="myapp_%s.log" % dateTag, level=logging.INFO,format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()

def logging_call(popenargs, **kwargs):
    process = subprocess.Popen(popenargs, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    def check_io():
           while True:
                output = process.stdout.readline().decode()
                if output:
                    logger.log(logging.INFO, output)
                else:
                    break

    # keep checking stdout/stderr until the child exits
    while process.poll() is None:
        check_io()

def main():
    start = time.perf_counter()
    # url="https://build-artifactory.eng.vmware.com/artifactory/taurus-generic-local/com/example/demoHelloWorld/demoHelloWorld-0.0.1-SNAPSHOT.jar"
    # path = ArtifactoryPath(url)
    #
    # with path.open() as fd:
    #     with open("demo.jar", "wb") as out:
    #         out.write(fd.read())

    with concurrent.futures.ProcessPoolExecutor() as executor:
         process1 = executor.submit(logging_call,['java', '-jar', 'demo.jar', 'ingestData', '--name', 'Maria', '--age', '29'])
         process2 = executor.submit(logging_call,['java', '-jar', 'demo.jar', 'ingestData', '--name', 'Maria1', '--age', '29'])
         logger.info(f'Return Value: {process1.result()}')
         logger.info(f'Return Value: {process2.result()}')
    end = time.perf_counter()
    logger.info(f'Finished in {round(end - start, 2)} second(s)')

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logging.warning("Stopping...")
